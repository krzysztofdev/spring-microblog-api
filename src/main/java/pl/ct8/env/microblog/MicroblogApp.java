package pl.ct8.env.microblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroblogApp {

	public static void main(String[] args) {
		SpringApplication.run(MicroblogApp.class, args);
	}

}
