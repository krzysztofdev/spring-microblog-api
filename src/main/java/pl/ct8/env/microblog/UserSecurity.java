package pl.ct8.env.microblog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.ct8.env.microblog.service.PostService;

@Component("userSecurity")
public class UserSecurity {
    @Autowired
    PostService postService;

    public boolean postOwner(Authentication authentication, int postId) {
        return postService.findById(postId).getUser().equals(authentication.getPrincipal());
    }

    public boolean principalIsUser(Authentication authentication, String username) {
        return username.equals(authentication.getName());
    }

}
