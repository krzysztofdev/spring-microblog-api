package pl.ct8.env.microblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ct8.env.microblog.model.Tag;
import pl.ct8.env.microblog.repository.TagRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    public Tag getTagByName(String name) {
        return tagRepository.findByName(name)
                .orElseGet(() -> tagRepository.save(new Tag(name)));
    }

    public Set<Tag> addTags(Set<String> tagsStrList) {
        Set<Tag> tags = new HashSet<>();
        tagsStrList.forEach(tagStr -> {
            tags.add(getTagByName(tagStr.toLowerCase()));
        });
        return tags;
    }
}
