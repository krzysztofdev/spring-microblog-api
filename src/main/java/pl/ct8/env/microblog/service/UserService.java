package pl.ct8.env.microblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.ct8.env.microblog.dto.UserRequest;
import pl.ct8.env.microblog.dto.UserResponse;
import pl.ct8.env.microblog.mapper.UserMapper;
import pl.ct8.env.microblog.model.User;
import pl.ct8.env.microblog.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.CONFLICT;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username \"" + username + "\" not found."));
    }

    public UserResponse get(String username) {
        return userMapper.userToUserResponse((User) loadUserByUsername(username));
    }

    public void registerUser(UserRequest userRequest) throws ResponseStatusException {
        if (userRepository.existsByEmail(userRequest.getEmail())) {
            throw new ResponseStatusException(CONFLICT, "Account with email \"" + userRequest.getEmail() + "\" already exists");
        }
        userRepository.save(convertToEntity(userRequest));
    }

    private User convertToEntity(UserRequest userRequest) {
        if (userRequest.getPassword() != null) {
            userRequest.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        }
        return userMapper.userRequestToUser(userRequest);
    }

    public void updateUser(String username, UserRequest userRequest) {
        User user = convertToEntity(userRequest);
        user.setUsername(username);
        userRepository.save(user);
    }

    public void delete(String username) {
        userRepository.deleteById(username);
    }

    public List<UserResponse> getAll() {
        List<UserResponse> list = new ArrayList<>();
        userRepository.findAll()
                .forEach(user -> list.add(userMapper.userToUserResponse(user)));
        return list;
    }
}