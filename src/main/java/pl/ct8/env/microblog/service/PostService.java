package pl.ct8.env.microblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.ct8.env.microblog.dto.PostRequest;
import pl.ct8.env.microblog.dto.PostResponse;
import pl.ct8.env.microblog.mapper.PostMapper;
import pl.ct8.env.microblog.model.Post;
import pl.ct8.env.microblog.model.Tag;
import pl.ct8.env.microblog.model.User;
import pl.ct8.env.microblog.repository.PostRepository;
import pl.ct8.env.microblog.util.TagUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private TagService tagService;

    @Autowired
    private PostMapper postMapper;

    @PersistenceContext
    private EntityManager entityManager;

    public List<PostResponse> getAll() {
        List<PostResponse> posts = new ArrayList<>();
        postRepository.findAll().forEach(post -> postMapper.postToPostResponse(post));
        return posts;
    }

    public Post findById(int id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find post"));
    }

    public PostResponse get(int id) {
        return postMapper.postToPostResponse(postRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find post"))
        );
    }

    private void add(Post post) {
        updateTags(post);
        postRepository.save(post);
    }

    public void add(PostRequest postRequest, String username) {
        Post post = convertToEntity(postRequest);
        post.setUser(entityManager.getReference(User.class, username));
        add(post);
    }

    public void add(int parentId, PostRequest postRequest, String username) {
        Post post = convertToEntity(postRequest);
        post.setUser(entityManager.getReference(User.class, username));
        post.setParent(entityManager.getReference(Post.class, parentId));
        add(post);
    }

    public void update(int id, PostRequest postRequest) {
        Post post = convertToEntity(postRequest);
        post.setId(id);
        updateTags(post);
        postRepository.save(post);
    }

    public void delete(int id) {
        postRepository.deleteById(id);
    }

    public List<PostResponse> getAllByTag(String tag) {
        return postRepository.findByTagsName(tag).stream()
                .map(postMapper::postToPostResponse).collect(Collectors.toList());
    }

    private void updateTags(Post post) {
        Set<Tag> tags = tagService.addTags(TagUtil.getTagsOutOfString(post.getContent()));
        post.setTags(tags);
    }

    public void like(int id, User user) {
        Post post = findById(id);
        post.addLike(user);
        postRepository.save(post);
    }

    public void unlike(int id, User user) {
        Post post = findById(id);
        post.getLikes().remove(user);
        postRepository.save(post);
    }

    private Post convertToEntity(PostRequest dto) {
        return postMapper.postRequestToPost(dto);
    }

    private PostResponse convertToDto(Post post) {
        return postMapper.postToPostResponse(post);
    }

    public List<PostResponse> replies(int parentId) {
        return postRepository.findByParentId(parentId)
                .stream().map(this::convertToDto).collect(Collectors.toList());
    }
}
