package pl.ct8.env.microblog.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ct8.env.microblog.model.Tag;

import java.util.Optional;

public interface TagRepository extends CrudRepository<Tag, Integer> {
    Optional<Tag> findByName(String name);
}
