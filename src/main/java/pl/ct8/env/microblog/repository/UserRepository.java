package pl.ct8.env.microblog.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ct8.env.microblog.model.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    Optional<User> findByUsername(String username);
    boolean existsByEmail(String email);
}
