package pl.ct8.env.microblog.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ct8.env.microblog.model.Post;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Integer> {
    List<Post> findByTagsName(String name);
    List<Post> findByParentId(int id);
}
