package pl.ct8.env.microblog.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.function.Function;

@Service
@PropertySource("classpath:security.properties")
public class JwtUtil {
    JwtBuilder jwtBuilder;

    @Value("jwt.secret_key")
    private String SECRET;

    public String generateToken(UserDetails userDetails) {
        LocalDateTime time = LocalDateTime.now();
        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(java.sql.Timestamp.valueOf(time))
                .setExpiration(java.sql.Timestamp.valueOf(time.plusDays(1)))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    public Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
    private boolean isTokenNonExpired(String token) {
        return extractExpiration(token).after(new Date(System.currentTimeMillis()));
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        return (isTokenNonExpired(token) && userDetails.getUsername().equals(extractUsername(token)));
    }

}
