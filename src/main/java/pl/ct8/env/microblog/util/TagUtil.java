package pl.ct8.env.microblog.util;

import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TagUtil {

    private static final Pattern matchTag = Pattern.compile("#\\w+");

    public static Set<String> getTagsOutOfString(String string) {
        return matchTag.matcher(string)
                .results()
                .map(MatchResult::group)
                .map(str -> str.substring(1).toLowerCase())
                .collect(Collectors.toSet());
    }
}
