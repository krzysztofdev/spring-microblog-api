package pl.ct8.env.microblog.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.ct8.env.microblog.dto.PostRequest;
import pl.ct8.env.microblog.dto.PostResponse;
import pl.ct8.env.microblog.model.Post;

@Mapper(uses = { UserMapper.class })
public interface PostMapper {
    PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

    PostResponse postToPostResponse(Post post);

    Post postRequestToPost(PostRequest postRequest);
}
