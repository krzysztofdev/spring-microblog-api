package pl.ct8.env.microblog.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.ct8.env.microblog.dto.UserRequest;
import pl.ct8.env.microblog.dto.UserResponse;
import pl.ct8.env.microblog.model.User;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserResponse userToUserResponse(User user);

    User userRequestToUser(UserRequest userRequest);
}
