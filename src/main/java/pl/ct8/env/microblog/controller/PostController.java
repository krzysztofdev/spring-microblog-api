package pl.ct8.env.microblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.ct8.env.microblog.dto.PostRequest;
import pl.ct8.env.microblog.dto.PostResponse;
import pl.ct8.env.microblog.model.User;
import pl.ct8.env.microblog.service.PostService;
import pl.ct8.env.microblog.service.UserService;

import java.security.Principal;
import java.util.List;

@RestController
public class PostController {
    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @GetMapping("/posts")
    public List<PostResponse> getAll() {
        return postService.getAll();
    }

    @GetMapping("/posts/{id}")
    public PostResponse get(@PathVariable int id) {
        return postService.get(id);
    }

    @PostMapping("/posts")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody PostRequest postRequest, Principal principal) {
        postService.add(postRequest, principal.getName());
    }


    @PutMapping("/posts/{id}")
    public void update(@PathVariable int id, @RequestBody PostRequest postRequest) {
        postService.update(id, postRequest);
    }

    @GetMapping("/posts/{parentId}/replies")
    public List<PostResponse> replies(@PathVariable int parentId) {
        return postService.replies(parentId);
    }

    @PostMapping("/posts/{parentId}/replies")
    @ResponseStatus(HttpStatus.CREATED)
    public void reply(@PathVariable int parentId, @RequestBody PostRequest postRequest, Principal principal) {
        postService.add(parentId, postRequest, principal.getName());
    }

    @DeleteMapping("/posts/{id}")
    public void delete(@PathVariable int id) {
        postService.delete(id);
    }

    @PostMapping("/posts/{id}/like")
    @ResponseStatus(HttpStatus.CREATED)
    public void like(@PathVariable int id, Principal principal) {
        postService.like(id, (User)userService.loadUserByUsername(principal.getName()));
    }

    @DeleteMapping("/posts/{id}/like")
    public void unlike(@PathVariable int id, Principal principal) {
        postService.unlike(id, (User)userService.loadUserByUsername(principal.getName()));
    }

    @GetMapping("/tags/{tag}")
    public List<PostResponse> getByTag(@PathVariable String tag) {
        return postService.getAllByTag(tag);
    }
}
