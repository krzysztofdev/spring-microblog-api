package pl.ct8.env.microblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.ct8.env.microblog.dto.UserRequest;
import pl.ct8.env.microblog.dto.UserResponse;
import pl.ct8.env.microblog.model.AuthenticationResponse;
import pl.ct8.env.microblog.service.UserService;
import pl.ct8.env.microblog.util.JwtUtil;

import java.util.List;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody UserRequest userRequest) {
        userService.registerUser(userRequest);
    }

    @GetMapping("/users")
    public List<UserResponse> getAll() {
        return userService.getAll();
    }

    @PutMapping("/users/{username}")
    public void update(@PathVariable String username, @RequestBody UserRequest userRequest) {
        userService.updateUser(username, userRequest);
    }

    @GetMapping("/users/{username}")
    public UserResponse get(@PathVariable String username) {
        return userService.get(username);
    }

    @DeleteMapping("/users/{username}")
    public void delete(@PathVariable String username) {
        userService.delete(username);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody UserRequest userRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userRequest.getUsername(), userRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(UNPROCESSABLE_ENTITY, e.getMessage());
        }
        String jwt = jwtUtil.generateToken(userService.loadUserByUsername(userRequest.getUsername()));
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
}
