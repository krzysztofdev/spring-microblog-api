package pl.ct8.env.microblog.dto;

public class PostRequest {
    private String content;

    public PostRequest() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
