package pl.ct8.env.microblog;

import org.junit.jupiter.api.Test;
import pl.ct8.env.microblog.util.TagUtil;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;

public class TagUtilTest {
    @Test
    void tagFinder() {
        String test = "test #tag1 ttt #tag2 #TaG3";
        Set<String> tagsOutOfString = TagUtil.getTagsOutOfString(test);
        assertThat(tagsOutOfString, hasItems("tag1", "tag2", "tag3"));
    }
}
